/*
 Navicat MySQL Data Transfer

 Source Server         : 本地MySql
 Source Server Type    : MySQL
 Source Server Version : 50640
 Source Host           : localhost:3306
 Source Schema         : pic_upload_servers

 Target Server Type    : MySQL
 Target Server Version : 50640
 File Encoding         : 65001

 Date: 09/04/2020 17:02:15
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pic
-- ----------------------------
DROP TABLE IF EXISTS `pic`;
CREATE TABLE `pic`  (
  `pic_id` int(11) NOT NULL AUTO_INCREMENT,
  `pic_uploader_id` int(11) NOT NULL COMMENT '上传者ID',
  `pic_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '图片名称',
  `pic_url_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片路径',
  `pic_upload_date` datetime(0) NOT NULL COMMENT '上传时间',
  `pic_md5` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'MD5校验码',
  `pic_size` int(11) NOT NULL COMMENT '图片大小',
  `pic_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '图片类型',
  `pic_remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '备注',
  `pic_status` int(255) NOT NULL COMMENT '图片状态',
  PRIMARY KEY (`pic_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pic
-- ----------------------------
INSERT INTO `pic` VALUES (9, 1, '007kPYPngy1gc27g41x7kg307u05k7wi', '\\upload_pic\\2020\\2\\14\\3ykkTaHk', '2020-03-13 23:09:47', '2605ceeaace62dc97c1c0e5f0f1268da', 2292, 'gif', '123sssss的撒旦asdss', 0);
INSERT INTO `pic` VALUES (12, 1, '007kPYPngy1gc27g41x7kg307u05k7wi', '\\upload_pic\\2020\\2\\14\\3ykkTaHk', '2020-03-14 13:58:22', '2605ceeaace62dc97c1c0e5f0f1268da', 2292, 'gif', '444aa', 1);
INSERT INTO `pic` VALUES (13, 1, '007kPYPngy1gc27g41x7kg307u05k7wi', '\\upload_pic\\2020\\2\\14\\3ykkTaHk', '2020-03-14 13:59:15', '2605ceeaace62dc97c1c0e5f0f1268da', 2292, 'gif', '5551', 1);
INSERT INTO `pic` VALUES (14, 1, '1b96ee70e20f0636', '\\upload_pic\\2020\\3\\8\\yGJ5JWwE', '2020-04-08 09:17:00', '6062fe53236aba7c74b150a1e6d2d23a', 190, 'gif', '天哥ASS', 0);
INSERT INTO `pic` VALUES (19, 1, '_1b96ee70e20f0636', '\\upload_pic\\2020\\3\\8\\yGJ5JWwE', '2020-04-08 09:54:20', '6062fe53236aba7c74b150a1e6d2d23a', 190, 'gif', NULL, 0);
INSERT INTO `pic` VALUES (20, 1, '_1b96ee70e20f0636', '\\upload_pic\\2020\\3\\8\\yGJ5JWwE', '2020-04-08 09:54:41', '6062fe53236aba7c74b150a1e6d2d23a', 190, 'gif', NULL, 0);
INSERT INTO `pic` VALUES (21, 4, '_1b96ee70e20f0636', '\\upload_pic\\2020\\3\\8\\yGJ5JWwE', '2020-04-09 08:53:28', '6062fe53236aba7c74b150a1e6d2d23a', 190, 'gif', NULL, 1);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_roles` int(255) NOT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'admin', 0);
INSERT INTO `user` VALUES (2, 'user', 'user', 1);
INSERT INTO `user` VALUES (3, '222', 'admin', 1);
INSERT INTO `user` VALUES (4, 'admin2', 'admin', 1);

SET FOREIGN_KEY_CHECKS = 1;
