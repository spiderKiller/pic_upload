package com.hongpeiming.picupload.service;

import com.hongpeiming.picupload.pojo.User;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    //通过ID查询用户
    User selectUserById(Integer userId);

    //通过账号查询
    User selectUserByAccount(String username);

    //账号是否存在
    boolean checkUserAccountRepeated(String UserAccount);

    //注册账号
    int register(User user);

    //密码验证
    boolean checkUserPass(String userAccount,String password);

}
