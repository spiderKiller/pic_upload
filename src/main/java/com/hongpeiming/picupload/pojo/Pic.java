package com.hongpeiming.picupload.pojo;

import java.util.Date;

public class Pic {
    private Integer picId;

    private Integer picUploaderId;

    private String picName;

    private String picUrlPath;

    private Date picUploadDate;

    private String picMd5;

    private Integer picSize;

    private String picType;

    private String picRemark;

    private Integer picStatus;

    public Integer getPicId() {
        return picId;
    }

    public void setPicId(Integer picId) {
        this.picId = picId;
    }

    public Integer getPicUploaderId() {
        return picUploaderId;
    }

    public void setPicUploaderId(Integer picUploaderId) {
        this.picUploaderId = picUploaderId;
    }

    public String getPicName() {
        return picName;
    }

    public void setPicName(String picName) {
        this.picName = picName == null ? null : picName.trim();
    }

    public String getPicUrlPath() {
        return picUrlPath;
    }

    public void setPicUrlPath(String picUrlPath) {
        this.picUrlPath = picUrlPath == null ? null : picUrlPath.trim();
    }

    public Date getPicUploadData() {
        return picUploadDate;
    }

    public void setPicUploadData(Date picUploadDate) {
        this.picUploadDate = picUploadDate;
    }

    public String getPicMd5() {
        return picMd5;
    }

    public void setPicMd5(String picMd5) {
        this.picMd5 = picMd5 == null ? null : picMd5.trim();
    }

    public Integer getPicSize() {
        return picSize;
    }

    public void setPicSize(Integer picSize) {
        this.picSize = picSize;
    }

    public String getPicType() {
        return picType;
    }

    public void setPicType(String picType) {
        this.picType = picType == null ? null : picType.trim();
    }

    public String getPicRemark() {
        return picRemark;
    }

    public void setPicRemark(String picRemark) {
        this.picRemark = picRemark == null ? null : picRemark.trim();
    }

    public Integer getPicStatus() {
        return picStatus;
    }

    public void setPicStatus(Integer picStatus) {
        this.picStatus = picStatus;
    }
}