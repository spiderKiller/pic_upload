
function addPicOnPage(PicData) {
    var data = PicData;
    //alert("data.length="+data.length);
    for (var i=0;i<data.length;i++)
    {
        //alert("data[i]"+data[i]);
        //alert("data[i].picId"+data[i].picId);
        var Layout = document.createElement('div');

        var uploadedpic = document.createElement('div');
        var img = document.createElement('img');

        var close = document.createElement('i');
        var copy = document.createElement('span');

        Layout.className = "picLayout";
        Layout.id = "picId"+data[i].picId;
        uploadedpic.className = "uploadedpic";
        img.src = "/loadpicfile/"+data[i].picId+"-"+data[i].picName+"."+data[i].picType;
        close.className = "layui-icon layui-icon-close";
        $(close).attr("onclick","deletepic("+data[i].picId+")");
        //close.click("deletepic("+data[i].picId+")");
        copy.textContent = "查看图片链接";
        $(copy).attr("onclick","copyurl("+data[i].picId+")");
        //copy.click("copyurl("+data[i].picId+")");

        uploadedpic.appendChild(img);
        Layout.appendChild(uploadedpic);
        Layout.appendChild(close);
        Layout.appendChild(copy);


        $("#uploadedlist").append(Layout);

    }

}

function deletepic(picId) {

    $.ajax({
        url:"/deletepic/"+picId,
        cache:false,
        //async: false,
        type:"post",
        success:function (msg) {
            layer.msg("删除成功！");
            $("#picId"+picId).remove();
        }
    });
}

function copyurl(picId) {
    var host = $("#contextPath").attr("href");
    var url = $("#picId"+picId+" .uploadedpic img").attr("src");
    //alert(host+url);
    layer.alert("请复制图片地址："+host+url)
}